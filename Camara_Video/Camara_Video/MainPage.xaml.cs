﻿using Plugin.Media;
using Plugin.Media.Abstractions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace Camara_Video
{
    public partial class MainPage : ContentPage
    {
        public MainPage()
        {
            InitializeComponent();
        }

        private async void tomarfoto(object sender, EventArgs e)
        {
            await CrossMedia.Current.Initialize();
            if (!CrossMedia.Current.IsTakePhotoSupported || !CrossMedia.Current.IsCameraAvailable)
            {
                await DisplayAlert("Ocurrio Algo","La camara no se detecto","OK");
                return;
            }

            var file=await CrossMedia.Current.TakePhotoAsync(
                new StoreCameraMediaOptions
                {
                   SaveToAlbum = true,
                   Directory = "MiDirectorio"
                }
            );
            if (file==null)
                return;

                mostrarimagen.Source = FileImageSource.FromStream(() =>
                {
                    var stream = file.GetStream();
                    file.Dispose();
                    return stream;
                });

            
        }

        private async void buscarfoto(object sender, EventArgs e)
        {
            await CrossMedia.Current.Initialize();

            if (!CrossMedia.Current.IsPickPhotoSupported)
            {
                await DisplayAlert("Ops", "Galeria de fotos no soportada", "OK");
                return;
            }
            var file = await CrossMedia.Current.PickPhotoAsync();
            if (file == null)
                return;

            mostrarimagen.Source = ImageSource.FromStream(() =>
           {
               var strem = file.GetStream();
               file.Dispose();
               return strem;
           });
        }

        private async void grabarvideo(object sender, EventArgs e)
        {
            await CrossMedia.Current.Initialize();
            if (!CrossMedia.Current.IsTakeVideoSupported || !CrossMedia.Current.IsCameraAvailable)
            {
                await DisplayAlert("Ops","No se detecto la camara","OK");
                return;
            }
            var file = await CrossMedia.Current.TakeVideoAsync(
                new StoreVideoOptions
                {
                    SaveToAlbum=true,
                    Directory = "Demo",
                    Quality = VideoQuality.Medium
                });
            if (file == null)
                return;

            mostrarimagen.Source = ImageSource.FromStream(() =>
            {
                var stream = file.GetStream();
                file.Dispose();
                return stream;
            });


        }

        private async void buscarvideo(object sender, EventArgs e)
        {
            await CrossMedia.Current.Initialize();

            if (!CrossMedia.Current.IsPickVideoSupported)
            {
                await DisplayAlert("Ops", "Galeria de videos", "Ok");
                return;
            }
            var file = await CrossMedia.Current.PickVideoAsync();
            if (file == null)
                return;

            mostrarimagen.Source = ImageSource.FromStream(() =>
                {
                    var stream = file.GetStream();
                    file.Dispose();
                    return stream;
                });
        }
    }
}
